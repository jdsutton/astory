DOCS_SOURCE=docs/source
DOCUMENTATION=documentation
DOCS_TMP=docs-tmp
ENV=environment
PIP=$(shell which pip)
SLURP=src/JSUtils/Slurp
SRC=src
DIST=dist
TEST_TEMP=./test-tmp
MOCHA=./node_modules/.bin/nyc ./node_modules/.bin/mocha
MOCHA_TEST_LOCATION='$(TEST_TEMP)/{,!(ui)/**}/test_*.js'
SUDO=$(shell which sudo)
DB_NAME=MyDB
CONFIG="config/myapp.config.json frontend_dev"
TEST_CONFIG=config/test.json
PYTHON=$(shell which python3)
PACKAGE=
STATIC_PAGES_SRC=
STATIC_PAGES_DIST=
MYSQL_PASSWORD=root

default: clean
	@make -s build_dev

	./main.py &
	cd $(DIST) && $(PYTHON) -m http.server 5000

prod: build_prod
	./main.py &
	cd $(DIST) && $(PYTHON) -m http.server 5000

build_dev:
	scripts/dev_build.sh $(SRC) $(DIST) $(CONFIG) &

run_dev_quiet: clean
	@# Runs the dev server quietly in the background.
	@scripts/dev_build.sh $(SRC) $(DIST) $(TEST_CONFIG) > /dev/null &

	@./main.py > /dev/null &
	@cd $(DIST) && $(PYTHON) -m http.server 5000 > /dev/null &

build_prod: clean
	@scripts/prod_build.sh $(SRC) $(DIST) $(STATIC_PAGES_SRC) $(STATIC_PAGES_DIST)

deploy: clean build_prod
	echo "TODO"

clean:
	@rm -rf $(DIST)
	@mkdir -p $(DIST)
	-@killall python3 &> /dev/null || true
	-@fuser -k 5000/tcp &> /dev/null
	-@fuser -k 5001/tcp &> /dev/null

essentials:
	$(SUDO) apt-get update
	$(SUDO) apt-get -y install curl git python3-setuptools wget zlib1g-dev openssl virtualenv python3-pip

cypress_dependencies:
	$(SUDO) apt-get -y install xvfb libgtk2.0-0 libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2

install: essentials submodule_init cypress_dependencies
	$(SUDO) apt-get update

	$(PIP) install --upgrade $(SLURP)
	$(PIP) install --upgrade .

	$(SUDO) apt-get -y install xvfb libgtk2.0-0 libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2

	# Install MySQL server.
	@echo "mysql-server mysql-server/root_password password $(MYSQL_PASSWORD)" | $(SUDO) debconf-set-selections
	@echo "mysql-server mysql-server/root_password_again password $(MYSQL_PASSWORD)" | $(SUDO) debconf-set-selections
	$(SUDO) apt-get -y install mysql-server

	$(SUDO) chmod -R 755 /var/lib/mysql/

	$(SUDO) apt-get update
	curl -sL https://deb.nodesource.com/setup_10.x | $(SUDO) bash -
	$(SUDO) apt-get install -y nodejs

	$(SUDO) /etc/init.d/mysql start

	npm install

lint:
	npx jshint $(SRC)

test: create_test_database
	$(PYTHON) `which nosetests` --nocapture --with-coverage --cover-package=$(PACKAGE) --processes=8 --cover-erase
	coverage report -m --skip-covered --omit=env

	make -s run_dev_quiet

	npx cypress run

	make clean

create_mysql_user:
	# This monstrosity fixes the pipelines.
	@mysql -u root -p$(MYSQL_PASSWORD) -e "UPDATE mysql.user SET plugin='mysql_native_password' WHERE User='root'; UPDATE mysql.user SET authentication_string=PASSWORD('$(MYSQL_PASSWORD)') WHERE User='root'; FLUSH PRIVILEGES;"

create_test_database:
	./scripts/init_local_database.sh $(DB_NAME)_test
	
create_dev_database:
	./scripts/init_local_database.sh $(DB_NAME)_dev

create_production_database:
	./scripts/init_local_database.sh $(DB_NAME)

submodule_init:
	git submodule update --init --recursive

init_docs:
	sphinx-quickstart

docs:
	rm -rf $(DOCUMENTATION)
	mkdir $(DOCUMENTATION)
	
	@# Creates JS documentation.
	node_modules/documentation/bin/documentation.js build --shallow --sort-order=alpha --format=html --output=$(DOCUMENTATION) $(SRC)

	# https://samnicholls.net/2016/06/15/how-to-sphinx-readthedocs/
	mkdir -p $(DOCUMENTATION)/python
	mkdir -p $(DOCS_SOURCE)

	sphinx-apidoc -o $(DOCS_SOURCE) .
	sphinx-build . $(DOCUMENTATION)/python

	rm -rf $(DOCS_SOURCE)
	rm -rf docs
	rm -rf _build
	rm -rf _static
	rm -rf _templates

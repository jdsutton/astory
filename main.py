#!/usr/bin/env python3

from pythonlambdaserver.LambdaServer import LambdaServerApp

def main():
    '''
    TODO.
    '''


    directory = './services'
    port = 5001

    LambdaServerApp.run([directory], port)

if __name__ == '__main__':
    main()

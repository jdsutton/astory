import('Species.js');
import('WorldLocation.js');

/**
 * @class
 */
class WorldLocationFactory {

    /**
     * @public
     */
    static produce(x, y, regionType) {
        let maxCharacters = 3;
        let location = new WorldLocation(x, y, regionType);

        if (this._MAX_CHARACTERS_BY_TYPE[location.type])
            maxCharacters = this._MAX_CHARACTERS_BY_TYPE[location.type];

        const numCharacters = Math.round(Random.range(0, maxCharacters));
        const validSpecies = Array.from(
            Species.listSpecies()
                .filter(s => s.hasHabitat(location.regionType)));

        for (let i = 0; i < numCharacters; i++) {
            const character = new Character()
                .setSpecies(Random.choice(validSpecies));

            location.characters.push(character);
        }

        return location;
    }
}

WorldLocationFactory._MAX_CHARACTERS_BY_TYPE = {
    [WorldLocation.TYPE.CITY]: 15,
    [WorldLocation.TYPE.WILDERNESS]: 3,
    [WorldLocation.TYPE.CAVE]: 2,
    [WorldLocation.TYPE.HOME]: 5,
    [WorldLocation.TYPE.RELIGIOUS_SITE]: 7,
    [WorldLocation.TYPE.FORTRESS]: 10,
    [WorldLocation.TYPE.FARM]: 4,
    [WorldLocation.TYPE.INDUSTRIAL_SITE]: 9,
    [WorldLocation.TYPE.ROAD]: 1,
    [WorldLocation.TYPE.MARKET]: 12,
    [WorldLocation.TYPE.LODGING]: 3,
};


/**
 * @class
 */
class StatChecker {

    /**
     * @public
     */
    static check(character, stats, handicap) {
        let base = 0;
        let statCount = 0;

        for (let stat of stats) {
            if (stat) {
                base += character.stats[stat];
                statCount++;
            }
        }

        base = handicap + (base / statCount);

        const value = base + Random.range(-2, 2);

        for (let outcome of Object.values(this.OUTCOME)) {
            if (outcome.inRange(value)) return outcome;
        }
    }
}

/**
 * @class
 */
StatChecker.Outcome = class {

    /**
     * @constructor
     */
    constructor(title, minValue, maxValue) {
        this.title = title;
        this._minValue = minValue;
        this._maxValue = maxValue;
    }

    /**
     * @public
     */
    inRange(value) {
        return value >= this._minValue && value < this._maxValue;
    }
}

StatChecker.OUTCOME = {
    CATASTROPHE: new StatChecker.Outcome('catastrophe', -Infinity, 0.5),
    TRADGEDY: new StatChecker.Outcome('tradgedy', 0.5, 2),
    MISFORTUNE: new StatChecker.Outcome('misfortune', 2, 4),
    NEUTRAL: new StatChecker.Outcome('neutral', 4, 6),
    POSITIVE: new StatChecker.Outcome('positive', 6, 8),
    VERY_LUCKY: new StatChecker.Outcome('very lucky', 8, 9.5),
    INCREDIBLE: new StatChecker.Outcome('incredible', 9.5, Infinity),
};

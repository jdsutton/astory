
/**
 * @class
 */
class Scenario {

    /**
     * @public
     */
    static generate() {
        return Random.choice(this._all)();
    }

    /**
     * @private
     */
    static _tales() {
        const area = Random.choice(Scenario._AREAS);

        return `Nearby townsfolk long to hear tales of ${area}.`;
    }

    /**
     * @private
     */
    static _creatureAttack() {
        const creature = Random.choice(Species.listCreatureSpecies());

        return `A wild ${creature.toString()} suddenly attacks!`;
    }

    /**
     * @private
     */
    static _fortunateFind() {
        return `You notice something glinting on the ground!`;
    }

    /**
     * @private
     */
    static _illness() {
        return Random.choice([
            `You are beginning to feel ill.`,
            `Your stomach churns.`,
            `Your forehead burns with fever.`,
        ]);
    }

    /**
     * @private
     */
    static _creepy() {
        return `You feel that something is watching you.`;
    }

    /**
     * @private
     */
    static _romantic() {
        return `Someone winked at you!`;
    }

    /**
     * @private
     */
    static _theft() {
        return `Suddenly your pack feels a little lighter. Is something missing?`;
    }

    /**
     * @private
     */
    static _rumors() {
        return Random.choice([
            `You hear strange tales of bizzare beasts in the west.`,
            `A stranger tells you a rumour about a bounty hunter in town.`,
            `You hear gossip about an affair!`,
        ]);
    }
}

Scenario._AREAS = [
    'the North',
    'a dangerous cavern',
    'the wild sea',
    'the plane beyond the edge of the world',
    'the desert',
];

Scenario._all = [
    Scenario._tales,
    Scenario._creatureAttack,
    Scenario._fortunateFind,
    Scenario._illness,
    Scenario._creepy,
    Scenario._romantic,
    Scenario._theft,
    Scenario._rumors,
];
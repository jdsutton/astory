import('EventLogger.js');
import('Scenario.js');
import('WorldLocation.js');

class World {
    
    /**
     * @constructor
     */
    constructor() {
        this.party = [new Character()];
        this._travelCallback = null;

        EventLogger.addEventDescription(
            `The year is ${World.currentYear} of the <b>${World.currentEra.toString()}</b>.`);

        this.setLocation(WorldMap.getLocation());

        World._current = this;
    }

    /**
     * @public
     */
    update() {
        this._currentLocation.update();

        if (Math.random() < World._SCENARIO_PROBABILITY) {
            EventLogger.addEventDescription('<b>' + Scenario.generate() + '</b>');
        }
    }

    /**
     * @public
     */
    getAllCharacters() {
        return Random.shuffle(this._currentLocation.characters.concat(this._party));
    }

    /**
     * @public
     */
    setLocation(location) {
        EventLogger.addEventDescription(location.describe());

        WorldMap.setLocation(location);

        this._currentLocation = location;
    }

    /**
     * @public
     */
    get location() {
        return this._currentLocation;
    }

    /**
     * @public
     */
    travel(direction) {
        EventLogger.addEventDescription(`You travel to the ${direction}.`);

        this.setLocation(WorldMap.getAdjacentLocation(direction));
        
        if (this._travelCallback)
            this._travelCallback();
    }

    /**
     * @public
     */
    onTravel(callback) {
        this._travelCallback = callback;

        return this;
    }

    /**
     * @public
     */
    static get() {
        return this._current;
    }
}

World.EventLog = class {

    /**
     * @constructor
     */
    constructor(description, logLevel) {
        this.description = description;
        this.logLevel = logLevel;
    }
};

World.EventLog.LOG_LEVEL = {
    VERBOSE: 0,
    INFO: 1,
};

World._SCENARIO_PROBABILITY = 0.005;
World._current = null;

World.Era = class {

    /**
     * @constructor
     */
    constructor(name, startYear, endYear) {
        this.name = name;
        this.startYear = startYear;
        this.endYear = endYear;
    }

    /**
     * @public
     */
    toString() {
        return this.name;
    }
}

World.ERA = {
    BRONZE_AGE: new World.Era('Bronze Age', -3000, -100),
    IRON_AGE: new World.Era('Iron Age', -1000, 0),
    MIDDLE_AGES: new World.Era('Middle Ages', 0, 1300),
    RENAISSANCE: new World.Era('Renaissance', 1300, 1600),
    EARLY_MODERN_AGE: new World.Era('Early Modern Age', 1600, 1760),
    INDUSTRIAL_AGE: new World.Era('Industrial Age', 1760, 1880),
    MACHINE_AGE: new World.Era('Machine Age', 1880, 1945),
    ATOMIC_AGE: new World.Era('Atomic Age', 1945, 1970),
    INFORMATION_AGE: new World.Era('Information Age', 1970, 2050),
    FUTURE_AGE: new World.Era('Future Age', 2050, 3000),
};

World.currentYear = Math.floor(Random.range(-3000, 2500));
World.currentEra = World.ERA.BRONZE_AGE

for (let era of Object.values(World.ERA)) {
    if (World.currentYear >= era.startYear
            && World.currentYear < era.endYear) {

        World.currentEra = era;

        break
    }
}

class Capability {

    /**
     * @public
     */
    isUsableByCharacter(character) {
        return true;
    }

    /**
     * @public
     */
    isUsableWithArguments(args) {
        return true;
    }

    /**
     * @public
     */
    getParameterTypes() {
        return [];
    }

    /**
     * @public
     */
    use() {

    }

    /**
     * @public
     */
    getTemplateString() {
        return 'do a thing';
    }
};

Capability.ALL = [];
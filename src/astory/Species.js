import('WorldLocation.js');

/**
 * @class
 */
class Species {

    /**
     * @constructor
     */
    constructor(name) {
        this.name = name;
        this.attackAbilities = [
            'a mean look',
        ];
        this._habitat = new Set(Object.values(WorldLocation.REGION_TYPE));
    }

    /**
     * @public
     */
    addAttack(name) {
        this.attackAbilities.push(name);

        return this;
    }

    /**
     * @public
     */
    toString() {
        return this.name;
    }

    /**
     * @public
     */
    hasHabitat(regionType) {
        return this._habitat.has(regionType);
    }

    /**
     * @public
     */
    setHabitat(regionTypes) {
        this._habitat = new Set(regionTypes);

        return this;
    }

    /**
     * @private
     */
    static _listLeaves(obj) {
        let result = [];

        let stack = Array.from(Object.values(obj));

        while (stack.length) {
            const item = stack.pop();

            if (item instanceof Species) {
                result.push(item);
            } else {
                for (let value of Object.values(item)) {
                    stack.push(value);
                }
            }
        }

        result.sort();

        return result;
    }

    /**
     * @public
     */
    static listSpecies() {
        return this._listLeaves(Species.ALL);
    }

    /**
     * @public
     */
    static getByName(name) {
        for (let species of this.listSpecies()) {
            if (species.name === name) return species;
        }

        return null;
    }

    /**
     * @public
     */
    static listCreatureSpecies() {
        let taxonomy = Object.assign({}, Species.ALL);
        delete taxonomy.HUMANOID;

        return this._listLeaves(taxonomy);
    }

    /**
     * @public
     */
    static listHumanoidSpecies() {
        return this._listLeaves(Species.ALL.HUMANOID);
    }
}

Species.ALL = {
    HUMANOID: {
        AUTOMATON: new Species('Automaton')
            .addAttack('mechanical blows')
            .setHabitat([WorldLocation.REGION_TYPE.PLATEU]),
        HUMAN: new Species('Human')
            .addAttack('whirling fists'),
        ANIMATE_OBJECT: new Species('Animate Object')
            .addAttack('heavy blows')
            .setHabitat([WorldLocation.REGION_TYPE.PLATEU]),
        FAE: {
            FAERIE: new Species('Faerie')
                .addAttack('faerie dust')
                .setHabitat([WorldLocation.REGION_TYPE.FOREST]),
            SPRITE: new Species('Sprite')
                .addAttack('dazzling lights')
                .setHabitat([WorldLocation.REGION_TYPE.FOREST]),
        },
        MOLE: {
            STONE_BURROWER_DWARF: new Species('Stone Burrower')
                .addAttack('a mining pick')
                .setHabitat([WorldLocation.REGION_TYPE.CAVE]),
            CAVERN_LAKE_DWELLER: new Species('Cavern Lake Dweller')
                .addAttack('a clammy grasp')
                .setHabitat([WorldLocation.REGION_TYPE.CAVE]),
        },
        SCALESKIN: new Species('Scaleskin')
            .addAttack('sharp claws')
            .setHabitat([WorldLocation.REGION_TYPE.SWAMP]),
        CRAB_PERSON: new Species('Crabperson')
            .addAttack('clacking claws')
            .setHabitat([WorldLocation.REGION_TYPE.SWAMP]),
    },
    CANINE: {
        DOG: new Species('Dog')
            .addAttack('a rabid bite'),
        DINGO: new Species('Dingo')
            .addAttack('quick chomps'),
        BEARDOG: new Species('Beardog')
            .addAttack('heavy paw swipes')
            .setHabitat([WorldLocation.REGION_TYPE.FOREST]),
        WOLF: new Species('Wolf')
            .addAttack('an intimidating growl')
            .setHabitat([WorldLocation.REGION_TYPE.FOREST]),
        Hyena: new Species('Hyena')
            .addAttack('ferocious biting')
            .setHabitat([WorldLocation.REGION_TYPE.WILDERNESS]),
    },
    FELINE: {
        HOUSE_CAT: new Species('House Cat')
            .addAttack('a sudden scratch'),
        CARACAL: new Species('Caracal')
            .addAttack('powerful claws')
            .setHabitat([WorldLocation.REGION_TYPE.PLAINS]),
    },
    REPTILE: {
        DRAGON: {
            FIREBELLY_DWARF: new Species('Firebelly Dwarf')
                .addAttack('belching of fireballs')
                .setHabitat([WorldLocation.REGION_TYPE.MOUNTAIN]),
            DRAGON_FAIRY: new Species('Dragon Fairy')
                .addAttack('cutting wings')
                .setHabitat([WorldLocation.REGION_TYPE.FOREST]),
        },
        LIZARD: new Species('Lizard'),
        TOAD: new Species('Toad')
            .setHabitat([WorldLocation.REGION_TYPE.SWAMP]),
        VELOCIPATOR: new Species('Velociraptor')
            .addAttack('massive talons')
            .setHabitat([WorldLocation.REGION_TYPE.WILDERNESS]),
    },
    BEAST: {
        NIGHTMARE: new Species('Nightmare')
            .setHabitat([WorldLocation.REGION_TYPE.OTHERWORLD]),
        ELEMENTAL: new Species('Elemental')
            .setHabitat([WorldLocation.REGION_TYPE.OTHERWORLD]),
        HORROR: new Species('Horror')
            .setHabitat([WorldLocation.REGION_TYPE.OTHERWORLD]),
    },
    INSECT: {
        MANTIS: new Species('Mantis'),
    },
    ARACHNID: {
        SPIDER: new Species('Spider'),
        TARANTULA: new Species('Tarantula'),
        GOLIATH_BIRDEATER: new Species('Goliath Birdeater')
            .setHabitat([WorldLocation.REGION_TYPE.JUNGLE]),
    },
    CRUSTACEAN: {
        CRAB: new Species('Crab')
            .setHabitat([WorldLocation.REGION_TYPE.OCEAN]),
        SPIDER_CRAB: new Species('Spider Crab')
            .setHabitat([WorldLocation.REGION_TYPE.OCEAN]),
        LOBSTER: new Species('Lobster')
            .setHabitat([WorldLocation.REGION_TYPE.OCEAN]),
        SHRIMP: new Species('Shrimp')
            .setHabitat([WorldLocation.REGION_TYPE.OCEAN]),
        MANTIS_SHRIMP: new Species('Mantis Shrimp')
            .setHabitat([WorldLocation.REGION_TYPE.OCEAN]),
    }
};

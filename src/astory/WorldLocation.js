import('/src/JSUtils/math/Perlin.js');
import('/src/JSUtils/math/Random.js');

/**
 * @class
 */
class WorldLocation {
    
    /**
     * @constructor
     */
    constructor(x, y, regionType) {
        this._x = x;
        this._y = y;

        this.regionType = regionType;
        this.type = Random.choice(Object.values(WorldLocation.TYPE));

        // Name the region.
        const namePart1 = WorldLocation._DESCRIPTORS_BY_REGION_TYPE[this.regionType] || ['Unknown'];
        const namePart2 = WorldLocation._DESCRIPTORS_BY_TYPE[this.type] || ['Area'];
        this.name = Random.choice(namePart1) + ' ' + Random.choice(namePart2);

        this.owner = null;
        this.characters = [];
        this._adjacentLocationsByCompassDirection = {};
    }

    /**
     * @public
     */
    update() {
        this.characters.forEach(c => c.update());
    }

    /**
     * @public
     */
    removeCharacter(character) {
        const index = this.characters.indexOf(character);

        if (index < 0) return;

        this.characters.splice(index, 1);
    }

    /**
     * @public
     */
    addCharacter(character) {
        this.characters.push(character);
    }

    /**
     * @public
     */
    describe() {
        let humanoidCount = 0;
        let owner = String(this.owner || 'no one');

        for (let c of this.characters) {
            if (c.isHumanoid) humanoidCount++;
        }

        const otherCount = this.characters.length - humanoidCount;

        return `Your location is: <b>${this.name}</b>, owned by ${owner}.`
            + `\nThis place is a ${this.type} in the ${this.regionType}.`
            + `\nThere are ${humanoidCount} humanoids here and ${otherCount} other creatures here.`;
    }
}

WorldLocation.WATER_TABLE = -0.2;

WorldLocation.COMPASS_DIRECTION = {
    NORTH: 'north',
    EAST: 'east',
    SOUTH: 'south',
    WEST: 'west',
}

WorldLocation.HIGH_REGION_TYPE = {
    MOUNTAIN: 'Mountains',
    PLATEAU: 'Plateau',
    OTHERWORLD: 'Otherworld',
};

WorldLocation.NORMAL_REGION_TYPE = {
    WILDERNESS: 'Wilderness',
    FOREST: 'Forest',
    JUNGLE: 'Jungle',
    PLAINS: 'Plains',
    SWAMP: 'Swamp',
    CAVE: 'Cave',
    // RIVER: 'River',
    // POND: 'Pond',
};

WorldLocation.AQUATIC_REGION_TYPE = {
    ISLAND: 'Island',
    BAY: 'Bay',
    LAKE: 'Lake',
    OCEAN: 'Ocean',
};

WorldLocation.REGION_TYPE = Object.assign(
    {},
    WorldLocation.HIGH_REGION_TYPE,
    WorldLocation.NORMAL_REGION_TYPE,
    WorldLocation.AQUATIC_REGION_TYPE,
);

WorldLocation.COLOR_BY_REGION_TYPE = {
    [WorldLocation.REGION_TYPE.WILDERNESS]: '#827717',
    [WorldLocation.REGION_TYPE.FOREST]: '#1B5E20',
    [WorldLocation.REGION_TYPE.MOUNTAIN]: '#6D4C41',
    [WorldLocation.REGION_TYPE.PLAINS]: '#FFECB3',
    [WorldLocation.REGION_TYPE.PLATEAU]: '#8D6E63',
    [WorldLocation.REGION_TYPE.SWAMP]: '#424242',
    [WorldLocation.REGION_TYPE.ISLAND]: '#DCEDC8',
    [WorldLocation.REGION_TYPE.BAY]: '#B3E5FC',
    [WorldLocation.REGION_TYPE.CAVE]: '#546E7A',
    [WorldLocation.REGION_TYPE.LAKE]: '#64B5F6',
    [WorldLocation.REGION_TYPE.OCEAN]: '#0D47A1',
    [WorldLocation.REGION_TYPE.RIVER]: '#039BE5',
    [WorldLocation.REGION_TYPE.POND]: '#4DD0E1',
    [WorldLocation.REGION_TYPE.OTHERWORLD]: '#C5CAE9',
};

WorldLocation._DESCRIPTORS_BY_REGION_TYPE = {
    [WorldLocation.REGION_TYPE.WILDERNESS]: [
        'Wild',
        'Barren',
        'Bald',
        'Scarred',
    ],
    [WorldLocation.REGION_TYPE.FOREST]: [
        'Green',
        'Black',
        'Dark',
        'Wooded',
        'Towering',
        'Never-end',
        'Forbidden',
    ],
    [WorldLocation.REGION_TYPE.MOUNTAIN]: [
        'Tall',
        'Skycrash',
        'Giant',
        'Rocky',
        'Iron',
        'Perching',
    ],
    [WorldLocation.REGION_TYPE.PLAINS]: [
        'Fielded',
        'Waving',
        'Endless',
        'Far',
        'Wide',
        'Rolling',
    ],
    [WorldLocation.REGION_TYPE.PLATEAU]: [
        'Table',
        'Solid',
        'Flat',
        'High',
    ],
    [WorldLocation.REGION_TYPE.SWAMP]: [
        'Muck',
        'Sticky',
        'Gurgling',
    ],
    [WorldLocation.REGION_TYPE.ISLAND]: [
        'Ideal',
        'Peaceful',
        'Tropic',
    ],
    [WorldLocation.REGION_TYPE.BAY]: [
        'Still',
        'Tepid',
    ],
    [WorldLocation.REGION_TYPE.CAVE]: [
        'Cool',
        'Shadow',
        'Spiny',
    ],
    [WorldLocation.REGION_TYPE.LAKE]: [
        'Blue',
        'Hidden',
    ],
    [WorldLocation.REGION_TYPE.OCEAN]: [
        'Deep',
        'Boundless',
        'Sunken',
    ],
    [WorldLocation.REGION_TYPE.RIVER]: [
        'Mighty',
        'Flowing',
        'Rushing',
    ],
    [WorldLocation.REGION_TYPE.POND]: [
        'Quiet',
        'Resting',
    ],
    [WorldLocation.REGION_TYPE.OTHERWORLD]: [
        'Alien',
        'Mysterious',
        'Shattered',
    ],
};

WorldLocation.TYPE = {
    CITY: 'City',
    HOME: 'Home',
    RELIGIOUS_SITE: 'Religious Site',
    FORTRESS: 'Fortress',
    FARM: 'Farm',
    INDUSTRIAL_SITE: 'Industrial Site',
    ROAD: 'Road',
    MARKET: 'Market',
    LODGING: 'Lodging',
    SHIP: 'Ship',
};

WorldLocation._DESCRIPTORS_BY_TYPE = {
    [WorldLocation.TYPE.CITY]: [
        'Town',
        'Province',
        'City',
        'Metropolis',
        'Settlement',
    ],
    [WorldLocation.TYPE.HOME]: [
        'Home',
        'House',
        'Dwelling',
        'Hovel',
        'Mansion',
        'Camp',
        'Shelter',
    ],
    [WorldLocation.TYPE.RELIGIOUS_SITE]: [
        'Church',
        'Temple',
        'Steeple',
        'Monument',
        'House of Worship',
        'Sacred Ground',
    ],
    [WorldLocation.TYPE.FORTRESS]: [
        'Tower',
        'Bunker',
        'Fort',
        'Castle',
    ],
    [WorldLocation.TYPE.FARM]: [
        'Grove',
        'Vineyard',
        'Hills',
        'Park',
    ],
    [WorldLocation.TYPE.INDUSTRIAL_SITE]: [
        'Factory',
        'Drillsite',
        'Foundry',
    ],
    [WorldLocation.TYPE.ROAD]: [
        'Stretch',
        'Route',
        'Road',
        'Highway',
    ],
    [WorldLocation.TYPE.MARKET]: [
        'Market',
        'Bazaar',
        'Exchange',
    ],
    [WorldLocation.TYPE.LODGING]: [
        'Hostel',
        'Respite',
        'Safehouse',
    ],
    [WorldLocation.TYPE.SHIP]: [
        'Cruiser',
        'Freighter',
        'Yacht',
    ],
};

/**
 * @class
 */
class AttackBehavior {

    /**
     * @public
     */
    static do(character, target) {
        const world = Driver.getViewDriver().world;
        let description;

        character.stats.mood.angry--;

        if (target) {
            for (let c of world.location.characters) {
                c.stats.mood.fear++;
                c.stats.mood.happy--;
                c.stats.mood.aroused--;
            }

            const defend = Math.random() < (Random.range(0, target.stats.agility) / 10);

            description = `${character.toString()} attacks ${target.toString()}`
                + ` with ${character.describeAttack()}!`;

            target.stats.hitpoints -= character.stats.strength;

            if (defend) {
                description = description
                    + ` ${target.toString()} defends with ${target.describeAttack()}!`;

                character.stats.hitpoints -= target.stats.strength;
            }
        } else {
            description = `${character.toString()} screams in frustration!`;
        }

        return description;
    }
}

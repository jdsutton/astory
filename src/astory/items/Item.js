
/**
 * @class
 */
class Item {

    /**
     * @constructor
     */
    constructor() {
        this.weight = 1;
        this.deadliness = 0;
        this.value = 0;
        this.nutrition = 0;
        this.consumable = true;

        this.functions = new Set([
            Item.FUNCTIONS.GIVE,
            Item.FUNCTIONS.TRADE,
            Item.FUNCTIONS.DROP,
        ]);
    }

    /**
     * @public
     */
    eat(byCharacter) {
        const world = Driver.getViewDriver().world;

        byCharacter.stats.mood.hungry -= this.nutrition;
        byCharacter.deleteInventoryItem(this);

        const description = `${byCharacter.toString()} ate ${this.toString()}.`;

        EventLogger.addEventDescription(description, World.EventLog.LOG_LEVEL.VERBOSE);
    }

    /**
     * @public
     */
    throwAt(fromCharacter, targetCharacter) {

    }

    /**
     * @public
     */
    hasFunction(f) {
        return this.functions.has(f);
    }

    /**
     * @public
     */
    toString() {
        return `a ${this.constructor.name}`;
    }
}

Item.FUNCTIONS = {
    USE: 'use',
    EAT: 'eat',
    GIVE: 'give',
    TRADE: 'trade',
    DROP: 'drop',
    THROW_AT: 'throw at',
    ATTACK_WITH: 'attack with',
};

/**
 * @class
 */
class Firecracker extends Item {

    /**
     * @constructor
     */
    constructor() {
        super();

        this.functions.add(Item.FUNCTIONS.USE);
    }

    /**
     * @public
     */
    use(byCharacter) {
        for (let char of WorldMap.getLocation().characters) {
            char.stats.mood.fear++;
        }

        EventLogger.addEventDescription(`${byCharacter.toString()} set off a firecracker!`);

        byCharacter.deleteInventoryItem(this);
    }
}

/**
 * @class
 */
class BookOfInsults extends Item {

    /**
     * @constructor
     */
    constructor() {
        super();

        this.functions.add(Item.FUNCTIONS.USE);
        this.consumable = false;
    }

    /**
     * @public
     */
    use(byCharacter, targetCharacter) {
        targetCharacter.stats.mood.angry++;

        EventLogger.addEventDescription(
            `${byCharacter.toString()} opens a book of insults`
            + ` and looks directly at ${targetCharacter.toString()} with a smirk: `
            + Random.choice(BookOfInsults._INSULTS)
        );
    }
}

BookOfInsults._INSULTS = [
    '&ldquo;Your mother has the toes of a seasick toad!&rdquo;',
    '&ldquo;Your height is comparable to that of a dungbeetle!&rdquo;',
    '&ldquo;Your outfit is an embarassment!&rdquo;',
]

/**
 * @class
 */
class HealingSalve extends Item {

    /**
     * @constructor
     */
    constructor() {
        super();

        this._hp = 5;
        this.functions.add(Item.FUNCTIONS.USE);
    }

    /**
     * @public
     */
    use(byCharacter) {
        byCharacter.stats.hitpoints = Math.min(
            byCharacter.stats.hitpoints + this._hp,
            byCharacter.stats.maxHitpoints,
        );

        EventLogger.addEventDescription(`${byCharacter.toString()} used a healing salve!`);

        byCharacter.deleteInventoryItem(this);
    }
}


/**
 * @class
 */
class Pebble extends Item {

    /**
     * @constructor
     */
    constructor() {
        super();

        this.deadliness = 0.1;

        this.functions.add(Item.FUNCTIONS.THROW_AT);
    }
}

/**
 * @class
 */
class Crumb extends Item {

    /**
     * @constructor
     */
    constructor() {
        super();

        this.nutrition = 0.25;

        this.functions.add(Item.FUNCTIONS.EAT);
    }
}


/**
 * @class
 */
class Morsel extends Crumb {

    /**
     * @constructor
     */
    constructor() {
        super();

        this.nutrition = 1;
    }
}

/**
 * @class
 */
class Meal extends Crumb {

    /**
     * @constructor
     */
    constructor() {
        super();

        this.nutrition = 2;
    }
}

/**
 * @class
 */
class Feast extends Crumb {

    /**
     * @constructor
     */
    constructor() {
        super();

        this.nutrition = 4;
    }
}
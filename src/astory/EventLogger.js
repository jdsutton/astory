
/**
 * @class
 */
class EventLogger {

    /**
     * @public
     */
    static addEventDescription(description, logLevel) {
        console.log(description);

        this._eventDescriptionQueue.push(
            new World.EventLog(description, logLevel));
    }

    /**
     * @public
     */
    static queuePause() {
        this._pauses.push(this._eventDescriptionQueue.length);
    }

    /**
     * @public
     */
    static flushEventQueue() {
        const end = this._pauses.shift() || this._eventDescriptionQueue.length;
        const result = this._eventDescriptionQueue.slice(0, end);

        this._eventDescriptionQueue = this._eventDescriptionQueue.slice(end, Infinity);

        for (let i in this._pauses) {
            this._pauses[i] -= end;
        }

        return result;
    }
}

EventLogger._eventDescriptionQueue = [];
EventLogger._pauses = [];
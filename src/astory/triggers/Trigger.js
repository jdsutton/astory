
/**
 * @class
 */
class Trigger {

    /**
     * @public
     */
    canFire() {
        return false;
    }

    /**
     * @public
     */
    tryFire() {
        if (!this.canFire()) return false;

        this.fire();

        return true;
    }

    /**
     * @public
     */
    fire() {
        
    }
}
import('../Trigger.js');

/**
 * @class
 */
class BehaviorTrigger extends Trigger {

    /**
     * @constructor
     */
    constructor(character) {
        super();
        
        this._character = character;
    }
}
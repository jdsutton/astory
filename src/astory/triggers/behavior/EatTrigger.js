import('BehaviorTrigger.js');

/**
 * @class
 */
class EatTrigger extends BehaviorTrigger {

    /**
     * @public
     */
    canFire() {
        this._character.stats.mood.hungry += this._character.stats.metabolism / 10;
        this._character.stats.mood.energy += this._character.stats.metabolism / 10;

        if (this._character.stats.mood.hungry > 5) {

            return true;
        }
    }

    /**
     * @public
     */
    fire() {
        let target = null;

        for (let item of this._character.inventory) {
            if (item.hasFunction(Item.FUNCTIONS.EAT)) {
                target = item;
                
                break;
            }
        }

        if (target) {
            target.eat(this._character);
        } else {
            const world = Driver.getViewDriver().world;
            const description = `${this._character.toString()} groans with hunger.`;

            EventLogger.addEventDescription(description);
        }
    }
}
import('/src/astory/behaviors/AttackBehavior.js');
import('BehaviorTrigger.js');

/**
 * @class
 */
class AttackTrigger extends BehaviorTrigger {

    /**
     * @public
     */
    canFire() {
        return this._character.stats.mood.angry
            > this._character.stats.agreeability;
    }

    /**
     * @public
     */
    fire() {
        const driver = Driver.getViewDriver();
        const world = World.get();
        const allTargets = Random.shuffle(world.location.characters.concat(world.party));

        let target;

        for (let c of allTargets) {
            if (c !== this._character) {
                target = c;

                break;
            }
        }

        if (world.party.indexOf(target) >= 0) {
            let component = new CombatSimulatorComponent()
                .setEnemy(this._character)
                .setCharacter(target);

            EventLogger.addEventDescription(component.render());
            EventLogger.queuePause();
        } else {
            const description = AttackBehavior.do(this._character, target);

            EventLogger.addEventDescription(description);

            if (!this.canFire())
                EventLogger.addEventDescription(`${this._character.toString()} has calmed down.`); 
        }
    }
}
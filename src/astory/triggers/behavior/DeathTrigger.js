import('BehaviorTrigger.js');

/**
 * @class
 */
class DeathTrigger extends BehaviorTrigger {

    /**
     * @public
     */
    canFire() {
        if (this._character.stats.mood.hungry > 10) {
            this._cause = 'hunger';

            return true;
        }

        if (this._character.stats.hitpoints <= 0) {
            this._cause = 'their injuries';

            return true;
        }
    }

    /**
     * @public
     */
    fire() {
        const world = Driver.getViewDriver().world;
        const description = `${this._character.toString()} has died due to ${this._cause}.`;

        world.location.removeCharacter(this._character);

        EventLogger.addEventDescription(description);
    }
}
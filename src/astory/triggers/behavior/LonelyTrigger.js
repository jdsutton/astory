import('BehaviorTrigger.js');

/**
 * @class
 */
class LonelyTrigger extends BehaviorTrigger {

    /**
     * @public
     */
    canFire() {
        const world = Driver.getViewDriver().world;

        for (let c of world.location.characters) {
            if (c !== this._character && c.isHumanoid === this._character.isHumanoid) {
                return false;
            }
        }

        return true;
    }

    /**
     * @public
     */
    fire() {
        this._character.stats.mood.happy--;

        const world = Driver.getViewDriver().world;
        const description = `${this._character.toString()} is feeling lonely.`;

        if (this._character.isHumanoid)
            EventLogger.addEventDescription(description);

        if (this._character.stats.mood.happy < 4) {
            this._character.travelInDirection(
                Random.choice(Object.values(WorldLocation.COMPASS_DIRECTION)));
        }
    }
}
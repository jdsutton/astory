import('BehaviorTrigger.js');

/**
 * @class
 */
class FleeTrigger extends BehaviorTrigger {

    /**
     * @public
     */
    canFire() {
        return this._character.stats.mood.fear > this._character.stats.courage;
    }

    /**
     * @public
     */
    fire() {
        const world = Driver.getViewDriver().world;
        let description;

        if (Math.random() < this._character.stats.agility) {
            this._character.stats.mood.fear = 0;

            description = `${this._character.toString()} has fled!`;

            EventLogger.addEventDescription(description);
            
            this._character.travelInDirection(
                Random.choice(Object.values(WorldLocation.COMPASS_DIRECTION)));
        } else {
            description = `${this._character.toString()} tried to flee, but stumbled!`;

            EventLogger.addEventDescription(description);
        }
        
    }
}
import('BehaviorTrigger.js');

/**
 * @class
 */
class HealTrigger extends BehaviorTrigger {

    /**
     * @public
     */
    canFire() {
        if (this._character.stats.hitpoints / this._character.stats.maxHitpoints > 0.5)
            return false;

        for (let item of this._character.inventory) {
            if (item instanceof HealingSalve) return true; 
        }

        return false;
    }

    /**
     * @public
     */
    fire() {
        let target = null;

        for (let item of this._character.inventory) {
            if (item instanceof HealingSalve) {
                target = item;
                
                break;
            }
        }

        if (target) {
            target.use(this._character);
        }
    }
}
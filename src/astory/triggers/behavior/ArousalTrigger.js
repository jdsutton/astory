import('BehaviorTrigger.js');

/**
 * @class
 */
class ArousalTrigger extends BehaviorTrigger {

    /**
     * @public
     */
    canFire() {
        const world = Driver.getViewDriver().world;

        if (!this._character.isHumanoid) return false

        for (let c of world.location.characters) {
            if (!c.isHumanoid || c === this._character) continue;
            
            if (Math.random() < c.stats.charisma / 100) {
                this._target = c;

                return true;
            }
        }

        return false;
    }

    /**
     * @public
     */
    fire() {
        const world = Driver.getViewDriver().world;

        this._character.stats.mood.arousal += 1;

        const description = `${this._character.toString()} is eyeing ${this._target.toString()} with desire.`;

        EventLogger.addEventDescription(description);
    }
}
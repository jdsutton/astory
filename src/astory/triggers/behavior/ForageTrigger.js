import('BehaviorTrigger.js');

/**
 * @class
 */
class ForageTrigger extends BehaviorTrigger {

    /**
     * @public
     */
    canFire() {
        for (let i of this._character.inventory) {
            if (i.hasFunction(Item.FUNCTIONS.EAT))
                return false;
        }
        
        return this._character.stats.mood.energy >= 1;
    }

    /**
     * @public
     */
    fire() {
        const world = Driver.getViewDriver().world;
        const p = (this._character.stats.intelligence + this._character.stats.endurance) / 20;
        let description = `${this._character.toString()} went out to forage for food.`;

        if (Math.random() < p) {
            const reward = Random.choice([
                new Crumb(),
                new Morsel(),
                new Meal(),
                new Feast(),
            ]);

            this._character.inventory.push(reward);

            description = description + ` ${this._character.toString()} found ${reward}!`;
        }

        this._character.stats.mood.energy--;

        EventLogger.addEventDescription(description);
    }
}
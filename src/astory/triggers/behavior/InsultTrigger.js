import('BehaviorTrigger.js');

/**
 * @class
 */
class InsultTrigger extends BehaviorTrigger {

    /**
     * @public
     */
    canFire() {
        if (!this._character.isHumanoid) return false;
        
        if (this._character.stats.agreeability > 2)
            return false;

        for (let item of this._character.inventory) {
            if (item instanceof BookOfInsults) {
                this._item = item;
             
                return true;
            } 
        }

        return false;
    }

    /**
     * @public
     */
    fire() {
        let target;

        for (let c of World.get().getAllCharacters()) {
            if (c !== this._character) {
                target = c;

                break;
            }
        }

        if (target) {
            this._item.use(this._character, target);
        } else {
            EventLogger.addEventDescription(
                `${this._character.toString()} is feeling rude, but can't think of anything to say.`);
        }
    }
}
import('Trigger.js');

/**
 * @class
 */
class PriorityTrigger extends Trigger {

    /**
     * @constructor
     */
    constructor(triggers) {
        super();

        this._triggers = triggers || [];
    }

    /**
     * @public
     */
    addTrigger(trigger) {
        this._triggers.push(trigger);

        return this;
    }

    /**
     * @public
     */
    canFire() {
        for (let trigger of this._triggers) {
            if (trigger.canFire()) return true;
        }

        return false;
    }

    /**
     * @public
     */
    fire() {
        for (let trigger of this._triggers) {
            if (trigger.tryFire()) return;
        }
    }
}
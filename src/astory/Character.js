import('triggers/behavior/ArousalTrigger.js');
import('triggers/behavior/AttackTrigger.js');
import('triggers/behavior/DeathTrigger.js');
import('triggers/behavior/EatTrigger.js');
import('triggers/behavior/FleeTrigger.js');
import('triggers/behavior/ForageTrigger.js');
import('triggers/behavior/HealTrigger.js');
import('triggers/behavior/InsultTrigger.js');
import('triggers/behavior/LonelyTrigger.js');
import('triggers/PriorityTrigger.js');
import('items/Item.js');
import('Species.js');
import('/src/JSUtils/math/Random.js');

class Character {

    /**
     * @constructor
     */
    constructor() {
        this.gender = Random.choice(Object.values(Character.GENDER));
        this.setSpecies(Random.choice(Species.listSpecies()));
        this.occupation = Random.choice(Object.values(Character.OCCUPATIONS));
        this.age = Math.floor(Random.range(0, 100));
        

        this.stats = new Character.Stats();
        this._state = Random.choice(Object.values(Character.STATE));

        const triggers = Random.shuffle(Character._ALL_BEHAVIOR_TRIGGERS)
            .map(T => new T(this));

        this._behaviorTrigger = new PriorityTrigger(triggers);
        this._eatTrigger = new EatTrigger(this);
        this._deathTrigger = new DeathTrigger(this);
        this.inventory = [];

        const numItems = Random.range(0, Character._DEFAULT_MAX_INVENTORY_ITEMS);

        for (let i = 0; i < numItems; i++) {
            const ItemType = Random.choice(Character._ALL_ITEMS);

            this.inventory.push(new ItemType());
        }

        Character.allCharacters.push(this);
    }

    /**
     * @private
     */
    _generateName() {
        if (this.isHumanoid) {
            this.givenName = Random.choice(Character.GIVEN_NAMES[this.gender]);
            this.familyName = Random.choice(Character.FAMILY_NAMES);
        } else {
            this.givenName = this.species.name;
            this.familyName = '';
        }
    }

    /**
     * @public
     */
    setSpecies(species) {
        this.species = species;

        this.isHumanoid = Character._HUMANOID_SPECIES.has(this.species);

        this._generateName();

        return this;
    }

    /**
     * @public
     */
    travelInDirection(direction) {
        const world = Driver.getViewDriver().world;
        const newLocation = WorldMap.getAdjacentLocation(direction);
        const description = `${this.toString()} went ${direction}.`;

        world.location.removeCharacter(this);
        newLocation.addCharacter(this);

        EventLogger.addEventDescription(description);
    }

    /**
     * @public
     */
    deleteInventoryItem(item) {
        const index = this.inventory.indexOf(item);

        if (index < 0) return;

        this.inventory.splice(index, 1);
    }

    /**
     * @public
     */
    update() {
        if (this._deathTrigger.tryFire()) return;

        this._eatTrigger.tryFire();
        this._behaviorTrigger.tryFire();
    }

    /**
     * @public
     */
    toString() {
        let result = this.givenName;

        if (this.isHumanoid) { 
            result = result + ' ' + this.familyName;
        }

        return result;
    }

    /**
     * @public
     */
    describeAttack() {
        return Random.choice(this.species.attackAbilities);
    }
}

Character.allCharacters = [];

Character._DEFAULT_MAX_INVENTORY_ITEMS = 12;

Character._ALL_BEHAVIOR_TRIGGERS = [
    ArousalTrigger,
    AttackTrigger,
    FleeTrigger,
    ForageTrigger,
    HealTrigger,
    InsultTrigger,
    LonelyTrigger,
];

Character._ALL_ITEMS = [
    BookOfInsults,
    Crumb,
    Morsel,
    Meal,
    Morsel,
    Feast,
    Pebble,
    Firecracker,
    HealingSalve,
];

Character.STATE = {
    IDLE: 'idle',
    CONVERSATION: 'conversation',
    MOVING: 'moving',
    EATING: 'eating',
    HYGINE: 'hygine',
    COMBAT: 'combat',
    WORK: 'work',
    SLEEP: 'sleep',
    LOVE: 'love',
};

Character._HUMANOID_SPECIES = new Set(
    Species.listHumanoidSpecies());

Character.GENDER = {
    MAN: 'Boy/Man',
    WOMAN: 'Girl/Woman',
    OTHER: 'Other',
};

Character.OCCUPATIONS = {
    MONK: 'Monk',
    PRIEST: 'Priest',
    CARPENTER: 'Carpenter',
    THIEF: 'Thief',
    MONARCH: 'Monarch',
    FARMER: 'Farmer',
    SAILOR: 'Sailor',
    PIRATE: 'Pirate',
    BARD: 'Bard',
    ADVENTURER: 'Adventurer',
    MERCHANT: 'Merchant',
    FREELANCER: 'Freelancer',
    SOLDIER: 'Soldier',
    ASSASSIN: 'Assassin',
    FLORIST: 'Florist',
};

Character.GIVEN_NAMES = {
    [Character.GENDER.MAN]: [
        'Andrew',
        'Charles',
        'David',
        'Elroy',
        'Frank',
        'Gustov',
        'Isaac',
        'Thomas',
        'Patrick',
    ],
    [Character.GENDER.WOMAN]: [
        'Allison',
        'Bethany',
        'Chloe',
        'Danielle',
        'Ellen',
        'Frankie',
        'Jane',
        'Patricia',
    ],
    [Character.GENDER.OTHER]: [
        'Pat',
        'Alex',
        'Sandy',
        'Riley',
        'Jordan',
        'Blake',
        'Hayden',
        'Taylor',
    ],
};

Character.FAMILY_NAMES = [
    'Allpeg',
    'Bastile',
    'Callae',
    'Dappen',
    'Effale',
    'Ghertz',
    'Hillback',
    'Iller',
    'Jackson',
    'Kinsley',
];

Character.Stats = class {
    
    /**
     * @constructor
     */
    constructor() {
        this.maxHitpoints = Math.round(Random.range(0, 10));
        this.hitpoints = this.maxHitpoints;
        this.intelligence = Math.round(Random.range(0, 10));
        this.strength = Math.round(Random.range(0, 10));
        this.endurance = Math.round(Random.range(0, 10));
        this.agility = Math.round(Random.range(0, 10));
        this.charisma = Math.round(Random.range(0, 10));
        this.courage = Math.round(Random.range(0, 10));
        this.luck = Math.round(Random.range(0, 10));
        this.regeneration = Math.round(Random.range(0, 10));
        this.metabolism = Math.round(Random.range(0, 10));

        this.wealth = Math.round(Random.range(0, 10));
        this.agreeability = Math.round(Random.range(0, 10));
        this.deception = Math.round(Random.range(0, 10));
        this.greed = Math.round(Random.range(0, 10));

        this.mood = {
            happy: Math.round(Random.range(0, 10)),
            angry: Math.round(Random.range(0, 10)),
            aroused: Math.round(Random.range(0, 10)),
            hungry: Math.round(Random.range(0, 10)),
            energy: Math.round(Random.range(0, 10)),
            fear: 0,
        };
    }
};

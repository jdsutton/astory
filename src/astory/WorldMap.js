import('WorldLocationFactory.js');
import('/src/JSUtils/graphics/Shapes.js');

/**
 * @class
 */
class WorldMap {

    /**
     * @public
     */
    static draw() {
        Shapes.clearCanvas();

        const coords = this.getCoordinates();

        for (
            let x = Math.max(0, coords.x - this._RANGE);
            x <= coords.x + this._RANGE && x < this.map.length;
            x++) {
            
            for (
                let y = Math.max(0, coords.y - this._RANGE);
                y <= coords.y + this._RANGE && y < this.map[x].length;
                y++) {

                const location = this.map[x][y];
                const region = location.regionType;
                const color = WorldLocation.COLOR_BY_REGION_TYPE[region];

                if (x === coords.x && y === coords.y
                    && Math.floor(new Date().getTime() / 1000) % 2) {
                    Shapes.fillStyle('#FF0000');
                } else {
                    Shapes.fillStyle(color);
                }

                Shapes.rect(
                    (x - coords.x + this._RANGE) * WorldMap._SCALE,
                    (y - coords.y + this._RANGE) * WorldMap._SCALE,
                    WorldMap._SCALE, WorldMap._SCALE);
            }
        }
    }

    /**
     * @public
     */
    static getAdjacentLocation(direction) {
        const offset = this._OPPOSITE_DIRECTION[direction];

        return this.map[this._x + offset.x][this._y + offset.y];
    }

    /**
     * @public
     */
    static setLocation(location) {
        this._x = location._x;
        this._y = location._y;

        const bgImage = this._BG_URI_BY_REGION_TYPE[location.regionType];

        if (bgImage) {
            document.body.parentElement.style.backgroundImage = 'url(\'' + bgImage + '\')';
        }
    }

    /**
     * @public
     */
    static getLocation() {
        return this.map[this._x][this._y];
    }

    /**
     * @public
     */
    static getCoordinates() {
        return {
            x: this._x,
            y: this._y,
        };
    }

    /**
     * @public
     */
    static _generateMap() {

        for (let i = 0; i < this._DEFAULT_MAP_SIZE; i++) {
            let row = [];

            for (let j = 0; j < this._DEFAULT_MAP_SIZE; j++) {
                const height = Perlin.get(i, j, 0.1);
                let regionType;

                if (height < WorldLocation.WATER_TABLE) {
                    regionType = Random.choice(Object.values(WorldLocation.AQUATIC_REGION_TYPE));
                } else if (height > 0.2) {
                    regionType = Random.choice(Object.values(WorldLocation.HIGH_REGION_TYPE));
                } else {
                    regionType = Random.choice(Object.values(WorldLocation.NORMAL_REGION_TYPE));
                }

                row.push(WorldLocationFactory.produce(i, j, regionType));
            }

            this.map.push(row);
        }
    }
}

WorldMap.map = [];
WorldMap._DEFAULT_MAP_SIZE = 64;
WorldMap._x = Math.round(WorldMap._DEFAULT_MAP_SIZE / 2);
WorldMap._y = Math.round(WorldMap._DEFAULT_MAP_SIZE / 2);
WorldMap._CANVAS_ID = 'visualization';
WorldMap._SCALE = 5;
WorldMap._RANGE = 8;

WorldMap._BG_URI_BY_REGION_TYPE = {
    [WorldLocation.REGION_TYPE.FOREST]: '/static/forest.jpg',
    [WorldLocation.REGION_TYPE.PLAINS]: '/static/plains.jpg',
    [WorldLocation.REGION_TYPE.CAVE]: '/static/cave.jpg',
    [WorldLocation.REGION_TYPE.SWAMP]: '/static/swamp.jpg',
    [WorldLocation.REGION_TYPE.WILDERNESS]: '/static/wilderness.jpg',
};

WorldMap._OPPOSITE_DIRECTION = {
    [WorldLocation.COMPASS_DIRECTION.NORTH]: {x: 0, y: -1},
    [WorldLocation.COMPASS_DIRECTION.EAST]: {x: 1, y: 0},
    [WorldLocation.COMPASS_DIRECTION.SOUTH]: {x: 0, y: 1},
    [WorldLocation.COMPASS_DIRECTION.WEST]: {x: -1, y: 0},
};

Shapes.useCanvas(WorldMap._CANVAS_ID);

WorldMap._generateMap();
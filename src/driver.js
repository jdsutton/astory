import('/src/astory/World.js');
import('/src/astory/Character.js');
import('/src/astory/WorldMap.js');
import('/src/astory/Scenario.js');
import('/src/astory/StatChecker.js');
import('/src/JSUtils/util/Driver.js');
import('/src/JSUtils/util/Page.js');
import('/src/JSUtils/util/Select.js');

/**
 * @class
 */
class HomepageViewDriver extends Driver {

    /**
     * @public
     */
    static init() {
        super.init();

        this.world = new World()
            .onTravel(() => {
                Dom.setContents(ID.STORY, '');

                this._initStatChecker();
            });

        this._statCheckerChars = null;
        this._combatSimulator = new CombatSimulatorComponent();

        this.update();

        this._initTools();
    }

    /**
     * @public
     */
    static onTravelButtonClicked(direction) {
        this.world.travel(direction);

        this.update();
    }

    /**
     * @public
     */
    static addStoryElement(element) {
        Dom.appendTo(ID.STORY, element);
    }

    /**
     * @public
     */
    static update() {
        this.world.update();

        const events = EventLogger.flushEventQueue();

        for (let event of events) {
            if (event.logLevel < World.EventLog.LOG_LEVEL.INFO) continue;

            this.addStoryElement(`<div>${event.description}</div>`);
        }

        WorldMap.draw();
    }

    /**
     * @private
     */
    static _initStatChecker() {
        let char = new Character();
        delete char.stats.mood;

        const select1 = new Select(
            'stat1',
            char.stats,
            false,
            false,
        ).render();

        Dom.setContents(ID.STAT_SELECT_1, select1);

        const select2 = new Select(
            'stat2',
            Object.assign({'': ''}, char.stats),
            false,
            false,
        ).render();

        Dom.setContents(ID.STAT_SELECT_2, select2);

        const allChars = this.world.party.concat(this.world.location.characters.slice());
        let chars = [];

        this._statCheckerChars = allChars.slice();

        for (let i in allChars) {
            chars[i] = allChars[i].toString();
        }

        const select = new Select(
            'character',
            chars,
            false,
            true,
        ).render();

        Dom.setContents(ID.STAT_CHARACTER_SELECT, select);
    }

    /**
     * @public
     */
    static _initTools() {
        const tabManager = new ToggleableTabManagerComponent(
            [
                'Character Builder',
                'Stat Checker',
                'Scenario Generator',
                'Combat Simulator',
                'Quest Reward',
                'Beastiary',
            ],
            [
                ID.CHARACTER_BUILDER,
                ID.STAT_CHECKER,
                ID.SCENARIO_GENERATOR,
                ID.COMBAT_SIMULATOR,
                ID.QUEST_REWARDS,
                ID.BEASTIARY,
            ],
        );

        Dom.setContents(ID.TAB_ROW, tabManager.render());

        tabManager.setOnShowTab(ID.STAT_CHECKER, () => {
            this._initStatChecker();
        })
        tabManager.setOnShowTab(ID.COMBAT_SIMULATOR, () => {
            this._combatSimulator.update();
        });

        Dom.setContents(ID.CHARACTER_FORM_CONTAINER, new CharacterFormComponent().render());

        tabManager.showTab(ID.CHARACTER_BUILDER);

        Dom.setContents(ID.COMBAT_SIMULATOR, this._combatSimulator.render());
    }

    /**
     * @public
     */
    static onToolsButtonClicked() {
        Dom.toggleClass(document.body, CLASS.SHOW_TOOLS);
    }

    /**
     * @public
     */
    static onGenerateScenarioButtonClicked() {
        const scenario = Scenario.generate();

        Dom.setContents(ID.SCENARIO, scenario);
    }

    /**
     * @public
     */
    static onStatCheckButtonClicked() {
        const data = Dom.getDataFromForm(ID.STAT_CHECKER);
        const character = this._statCheckerChars[parseInt(data.character)];

        const result = StatChecker.check(
            character,
            [data.stat1, data.stat2],
            parseInt(data.difficulty),
        );

        Dom.setContents(ID.STAT_CHECK_OUTCOME, `<b>Outcome:</b> ${result.title}`);
    }
}

Page.addLoadEvent(() => {
    HomepageViewDriver.init();
});

const ID = {
    BEASTIARY: 'beastiary',
    COMBAT_SIMULATOR: 'combatSimulator',
    CHARACTER_BUILDER: 'characterBuilder',
    CHARACTER_FORM_CONTAINER: 'characterFormContainer',
    QUEST_REWARDS: 'questRewards',
    SCENARIO: 'scenario',
    SCENARIO_GENERATOR: 'scenarioGenerator',
    STAT_CHARACTER_SELECT: 'statCharacterSelect',
    STAT_CHECKER: 'statChecker',
    STAT_CHECK_OUTCOME: 'statCheckOutcome',
    STAT_SELECT_1: 'statSelect1',
    STAT_SELECT_2: 'statSelect2',
    STORY: 'story',
    TAB_ROW: 'tabRow',
    TEMPLATE: {
        
    }
};

const CLASS = {
    SHOW_TOOLS: 'ShowTools',
};

import('/src/astory/behaviors/AttackBehavior.js');
import('/src/JSUtils/components/Component.js');

/**
 * @class
 */
class CombatSimulatorComponent extends Component {

    /**
     * @constructor
     */
    constructor() {
        super(CombatSimulatorComponent.ID.TEMPLATE.THIS, {});

        this._enemy = null;
        this._selectedCharacterIndex = 0;
        this._uiDisabled = false;
        this._description = '';

        this._initParams();
    }

    /**
     * @public
     */
    setEnemy(enemy) {
        this._enemy = enemy;

        this.update();

        return this;
    }

    /**
     * @public
     */
    setCharacter(character) {
        this._selectedCharacterIndex = World.get().party.indexOf(character);

        this.update();

        return this;
    }

    /**
     * @private
     */
    _initParams() {
        let newParams = {
            description: this._description,
        };

        if (this._enemy) {
            newParams.enemy = `<td>${this._enemy.toString()}</td>`
                + `<td>${Math.round(this._enemy.stats.hitpoints)}</td>`
                + `<td>${Math.round(this._enemy.stats.strength)}</td>`
                + `<td>${Math.round(this._enemy.stats.agility)}</td>`;
        } else {
            newParams.enemy = '';
        }

        const party = Driver.getViewDriver().world.party;
        const selectedCharacter = party[this._selectedCharacterIndex];

        if (selectedCharacter) {
            newParams.character = `<td>${selectedCharacter.toString()}</td>`
                + `<td>${Math.round(selectedCharacter.stats.hitpoints)}</td>`
                + `<td>${Math.round(selectedCharacter.stats.strength)}</td>`
                + `<td>${Math.round(selectedCharacter.stats.agility)}</td>`;;
        } else {
            newParams.character = '';
        }

        if (this._uiDisabled) {
            newParams.disabled = 'disabled';
        } else {
            newParams.disabled = '';
        }

        Object.assign(this._params, newParams);
    }

    /**
     * @public
     */
    onGenerateEnemyButtonClicked() {
        const enemy = new Character();

        // TODO: Difficulty.

        this._enemy = enemy;

        this.update();
    }

    /**
     * @public
     */
    onFightButtonClicked() {
        const character = Driver.getViewDriver().world.party[this._selectedCharacterIndex];

        this._description = AttackBehavior.do(character, this._enemy);
        this._uiDisabled = true;

        if (this._enemy.stats.hitpoints <= 0) {
            this._description = this._description
                    + '<br>You won the battle!';

            Driver.getViewDriver().update();
        } else {
            setTimeout(() => {
                this._description = this._description
                    + '<br>' + AttackBehavior.do(this._enemy, character);
                this._uiDisabled = false;

                this.update();

            }, CombatSimulatorComponent._UI_TIMEOUT_MS);
        }

        this.update();
    }
}

CombatSimulatorComponent._UI_TIMEOUT_MS = 1000;

CombatSimulatorComponent.ID = {
    TEMPLATE: {
        THIS: 'CombatSimulatorComponent_template',
    },
};

import('/src/JSUtils/components/Component.js');

/**
 * @class
 */
class CharacterFormComponent extends Component {

    /**
     * @constructor
     */
    constructor() {
        const character = new Character();

        const params = Object.assign(
            {},
            character,
            character.stats,
            {
                speciesSelect: new Select(
                    'species',
                    Species.listSpecies(),
                    true,
                    true,
                )
                    .setSelected(character.species)
                    .render(),
                genderSelect: new Select(
                    'gender',
                    Character.GENDER,
                    true,
                    true,
                )
                    .setSelected(character.gender)
                    .render(),
            }
        );
        
        super(CharacterFormComponent.ID.TEMPLATE.THIS, params);
    }

    /**
     * @private
     */
    _getCharacter() {
        const data = Dom.getDataFromForm(this.getId());
        let result = new Character();

        result.givenName = data.givenName;
        result.familyName = data.familyName;
        result.gender = data.gender;
        result.age = parseInt(data.age || 0);
        result.setSpecies(Species.getByName(data.species));

        result.stats.maxHitpoints = parseInt(data.maxHitpoints || 0);
        result.stats.hitpoints = parseInt(data.maxHitpoints || 0);
        result.stats.intelligence = parseInt(data.intelligence || 0);
        result.stats.strength = parseInt(data.strength || 0);
        result.stats.endurance = parseInt(data.endurance || 0);
        result.stats.agility = parseInt(data.agility || 0);
        result.stats.charisma = parseInt(data.charisma || 0);
        result.stats.courage = parseInt(data.courage || 0);
        result.stats.luck = parseInt(data.luck || 0);
        result.stats.regeneration = parseInt(data.regeneration || 0);
        result.stats.metabolism = parseInt(data.metabolism || 0);
        result.stats.wealth = parseInt(data.wealth || 0);
        result.stats.agreeability = parseInt(data.agreeability || 0);
        result.stats.deception = parseInt(data.deception || 0);
        result.stats.greed = parseInt(data.greed || 0);

        return result;
    }

    /**
     * @public
     */
    onAddToLocationButtonClicked() {
        const character = this._getCharacter();

        Driver.getViewDriver().world.location.characters.push(character);
    }

    /**
     * @public
     */
    onAddToPartyButtonClicked() {
        const character = this._getCharacter();

        Driver.getViewDriver().world.party.push(character);
    }
}

CharacterFormComponent.ID = {
    TEMPLATE: {
        THIS: 'CharacterFormComponent_template',
    },
};
